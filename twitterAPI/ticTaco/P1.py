# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 09:37:49 2020

@author: Conor
"""
import os
import json
import tweepy
import time
import threading
import re
import random
import queue
# q = queue.Queue()
class database:
    def __init__(self):
        self.__db = {}
    def addItem(self,key,value):
        self.__db[key] = value
    def removeItem(self,key):
        del self.__df[key]
    def findItem(self,key):
        return self.__db[key]
    def getKeys(self):
        return self.__db.keys()

live = database()


class API:
    def __init__(self):
        self.__path = 'C:\\Users\\Conor\\Desktop'
        self.__setupAPI()
    def __setupAPI(self):
        cwd = os.getcwd()
        os.chdir(self.__path)
        with open('jsonkeys.json', 'r') as fp:
            keys = json.load(fp)
        try:
            self.__apik = keys['APIK']
            self.__apisk = keys['APISK']
            self.__bk = keys['BK']
            self.__at = keys['AT']
            self.__ats = keys['ATS']
            auth = tweepy.OAuthHandler(self.__apik, self.__apisk)
            auth.set_access_token(self.__at, self.__ats)
            
            self.__api = tweepy.API(auth,wait_on_rate_limit =True,wait_on_rate_limit_notify=(True))
            self.__un = self.__api.me().screen_name
            self.__id = self.__api.me().id
            print('API Launching...')
        except:
            print('ERROR IN LAUNCHING API!')
        os.chdir(cwd)
    def getUN(self):
        return self.__un
    def getID(self):
        return self.__id
    def getAPI(self):
        return self.__api

class tweetListener(API):
    def __init__(self):
        API.__init__(self)            
        self.__list = []
        self.lock = threading.Lock()
        self.runn = threading.Thread(target = self.run, args = ())
        self.runn.start()
    def run(self):
        while True:
            c_list = []
            for i in c_list:
                # q.remove(i)
                print(i)
                time.sleep(1)
            time.sleep(1)
    def additem(self,item):
        self.__list.append(item)

tweeter = tweetListener()
threads = []
class LIVE:
    def __init__(self):
        self.__val = 0
    def inc(self):
        self.__val += 1
    def reset(self):
        self.__val = 0
    def current(self):
        return self.__val
class tweetMain(API):
    def __init__(self):
        API.__init__(self)
        self.__api = self.getAPI()
        self.__active = True
        self.__x = 'X'
        self.__o = 'O'
        self.__known = []
        self.__main()

    def __main(self):
        self.__checkGame()
        while self.__active:
            twt = {}
            print('Listening...')
            self.__checkResponses(self.__tweetLast)
            for i in self.__resp:
                js = self.__buildJSON(i._json)
                self.__tweetLast = max(js['id'],self.__tweetLast)
                if js['rid'] == self.__tweetMain: #It is in response to game tweet!
                    if js['un'] not in self.__known: 
                        print('FROM: @{}, {}'.format(js['un'],js['text']))
                        if len(js['text']) == 1:
                            js['game'] = '1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9'
                            js = self.__player(js)
                            js = self.__computer(js)
                            twt['tun'] = f"@{js['un']}"
                            twt['text'] = js['game']
                            twt['tid'] = js['id']
                            self.__sendTweet(twt)
                            self.__startGameThread(js)
                        else:
                            #Invalid input
                            twt['tun'] = f"@{js['un']}"
                            twt['text'] = 'Not a Valid Input!'
                            twt['tid'] = js['id']
                            self.__sendTweet(twt)
            self.__flipActive()                
            time.sleep(15)
    def __newGame(self,js):
        try:
            print('Starting thread for @{}'.format(js['un']))
            log = LIVE()
            self.__known.append(js['un'])
            while self.__active:
                # print(self.__active)
                print('Waiting for @{}:{}...'.format(js['un'],log.current()))
                #Check for a response
                
                
                log.inc()
                if log.current() == 5:
                    self.__known.remove(js['un'])
                    
                    break
                time.sleep(15)
        except:
            print('Error: ',js)
            self.__known.remove(js['un']) 
            return
            
            
    def __moves(self,js):
        board = re.sub('\D','',js['game'])
        return list(board)
    def __win(self,js):
        win = re.sub('\W','',js['game'])
        # print(win)
        if self.__o*3 in win:
            print('win hori o')
        if self.__x*3 in win:
            print('win hori x')
        for i in range(0,3):
            if win[0+i:1+i] == win[3+i:4+i] and win[0+i:1+i] == win[6+i:7+i] and win[0+i:1+i] == self.__o:
                print('win vert o')
            if win[0+i:1+i] == win[3+i:4+i] and win[0+i:1+i] == win[6+i:7+i] and win[0+i:1+i] == self.__x:
                print('win vert x')
        di1 = win[0:1]+win[4:5]+win[8:9]
        di2 = win[2:3]+win[4:5]+win[6:7]
        if self.__o*3 == di1 or self.__o*3 == di2:
            print('win dia o')
        if self.__x*3 == di1 or self.__o*3 == di2:
            print('win dia x')

    def __player(self,js):
        moves = self.__moves(js)
        if js['text'] in moves:
            js['game'] = js['game'].replace(js['text'],self.__o)
            self.__win(js)
        else:
            twt = {}
            twt['tun'] = f"@{js['un']}"
            twt['text'] = 'Not a Valid Move!'
            twt['tid'] = js['id'] 
            self.__sendTweet(twt)
        return js
    def __computer(self,js):
        moves = self.__moves(js)
        pick = random.choice(moves)
        js['game'] = js['game'].replace(pick,self.__x)
        self.__win(js)
        return js
    def __sendTweet(self,twt):
        print(twt)
    def __startGameThread(self,js):
        t = threading.Thread(target=self.__newGame, args = [js])
        print(threading.active_count())
        # thread = threading.Thread(target = self.__newGame, args = [js])
        # thread.start()
        t.start()
        threads.append(t)
        
        
        
    def __buildJSON(self,json): # We only care about a few values so lets remove the others
        js = {}
        # print(json)
        js['uid'] = json['user']['id']
        js['un'] = json['user']['screen_name']
        js['id'] = json['id']
        js['rid'] = json['in_reply_to_status_id']
        js['run'] = json['in_reply_to_screen_name']
        js['text'] = json['full_text'].replace(f"@{self.getUN()}",'').replace(' ','').replace('\n','')
        return js
    
    
    def __flipActive(self):
        if self.__active == True:
            self.__active = False
        else:
            self.__active = True
    def __checkResponses(self,last):
        self.__resp = tweepy.Cursor(self.__api.search, q=f'@{self.getUN()}', since_id=last, tweet_mode='extended').items()
    def __checkGame(self): # See if a game exists
        file = 'C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport'
        try:
            with open(f'{file}/tweetMain.json', 'r') as fp:
                jsonTweet = json.load(fp)
            with open(f'{file}/tweetLast.json', 'r') as fp:
                jsonLast = json.load(fp)
            self.__tweetMain = jsonTweet['tweet']
            # self.__tweetLast = jsonLast['last']
            self.__tweetLast = self.__tweetMain
            self.__api.get_status(self.__tweetMain)
        except:
            self.__api.update_status('''Respond with a number to play!\n1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9''')
            for tweet in self.__api.user_timeline(screen_name = self.__un,count=1):
                tweetMain = tweet._json['id']
            jsonTweet = {'tweet':tweetMain}
            jsonLast = {'last':tweetMain}
            with open(f'{file}/tweetMain.json', 'w') as fp:
                json.dump(jsonTweet, fp)
            with open(f'{file}/tweetLast.json', 'w') as fp:
                json.dump(jsonLast, fp)
            self.__tweetMain = jsonTweet['tweet']
            self.__tweetLast = jsonLast['last']
            print("Tweet doesnt exist... Creating new tweet!")
    
game = tweetMain()



















