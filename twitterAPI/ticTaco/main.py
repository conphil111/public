# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 20:08:09 2020

@author: Conor
"""

import threading
import time
import os
import pandas as pd
import tweepy
import pickle
import json as json1
import random
import sys
    
class ticTaco:
    def __init__(self,path):
        self.__path = path
        self.__condition = ''
        self.__active = False
        self.__x = 'X'
        self.__o = 'O'
        self.__startAPI()
        self.monitor = threading.Thread(target = self.__monitorDM, args = ())
        self.monitor.start()
        # self.__main()
    def __startAPI(self):
        #Fucntion to load in the keys and set the api up
        cwd = os.getcwd()
        os.chdir(self.__path)
        with open('jsonkeys.json', 'r') as fp:
            keys = json1.load(fp)
        try:
            self.__apik = keys['APIK']
            self.__apisk = keys['APISK']
            self.__bk = keys['BK']
            self.__at = keys['AT']
            self.__ats = keys['ATS']
            auth = tweepy.OAuthHandler(self.__apik, self.__apisk)
            auth.set_access_token(self.__at, self.__ats)
            
            self.__api = tweepy.API(auth,wait_on_rate_limit =True,wait_on_rate_limit_notify=(True))
            self.__un = self.__api.me().screen_name
            self.__id = self.__api.me().id
            self.__started = {}
            print('API Launching...')
        except:
            print('ERROR IN LAUNCHING API!')
        os.chdir(cwd)
    def __activeGame(self):
        ## Try to start a game or see if a game exists.
        try:
            with open('C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport/tweetMain.json', 'r') as fp:
                jsonTweet = json1.load(fp)
            with open('C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport/tweetLast.json', 'r') as fp:
                jsonLast = json1.load(fp)
            #If we got a key, check if that tweet exists?
            self.__tweetMain = jsonTweet['tweet']
            self.__tweetLast = jsonLast['last']
            self.__api.get_status(self.__tweetMain)
            print('Tweet exists and reinitializing...')
        except:
            self.__api.update_status('''Respond with a number to play!\n1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9''')
            for tweet in self.__api.user_timeline(screen_name = self.__un,count=1):
                tweetMain = tweet._json['id']
            jsonTweet = {'tweet':tweetMain}
            jsonLast = {'last':tweetMain}
            with open('C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport/tweetMain.json', 'w') as fp:
                json1.dump(jsonTweet, fp)
            with open('C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport/tweetLast.json', 'w') as fp:
                json1.dump(jsonLast, fp)
            self.__tweetMain = jsonTweet['tweet']
            self.__tweetLast = jsonLast['last']
            print("Tweet doesnt exist... Creating new tweet!")
    def __flipActive(self):
        if self.__active == True:
            self.__active = False
        elif self.__active == False:
            self.__active = True  
    def __changeCondition(self,nc):
        self.__api.list_direct_messages(1); # I've sent myself a DM so I want to remove it from the queue
        self.__condition = nc
        time.sleep(60)  
    def __monitorDM(self):
        print('Monitor Launched...')
        while True:
            print('toc...')
            dms = self.__api.list_direct_messages(1) # Pull one of the new DMs
            for m in dms:
                nc = m.message_create['message_data']['text'].replace(' ','').replace('\n','').lower() # What is the text in the DM? Remove spaces and make all lowercase.
                t = m.message_create['target']['recipient_id']
                s = m.message_create['sender_id']
            if s==t: # Make sure this is a DM to myself. (High Risk for DDOS)
                if nc == 'start':
                    if self.__active == False:
                        self.__api.send_direct_message(self.__id, 'System told to start...') 
                        self.main = threading.Thread(target = self.__main, args = ())
                        self.main.start()
                        self.__changeCondition(nc)
                    else:
                        self.__api.send_direct_message(self.__id, 'System already initialized...')
                        nc = self.__condition
                        self.__changeCondition(nc)
                elif nc == 'stop' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to stop...') # Verify command change.
                    self.__changeCondition(nc)
                elif nc == 'break' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to break...') # Verify command change.
                    self.__changeCondition(nc)
                elif nc == 'critstop':
                    nc = 'break'
                    print('YIKES!')
                    self.__api.send_direct_message(self.__id, 'System told to CRITSTOP!!!')
                    self.__changeCondition(nc)
                    break
            time.sleep(60) 
    def __main(self):
        print('Main Launched...')
        try:
            self.__flipActive()
            self.__activeGame()
            while True:
                # Apply those rules!
                if self.__condition == 'start':
                    #Check if there are new responses!
                    self.__heartbeat()
                    self.__findNew()
                    time.sleep(5)
                elif self.__condition == 'stop':
                    pass
                elif self.__condition == 'break':
                    self.__flipActive()
                    break
        except:
            print('Error...\n',sys.exc_info()[0])
            
            self.__flipActive()
    def __available(self,s):
        s = s.replace('+','').replace(' ','').replace('|','').replace('-','').replace('\n','').replace('️','')
        s = s.replace(self.__x,'')
        s = s.replace(self.__o,'')
        s = list(s)
        return s
    def __heartbeat(self):
        print('tic...')
        
    def __findNew(self):
        resp = tweepy.Cursor(self.__api.search, q=f'@{self.__un}', since_id=self.__tweetLast, tweet_mode='extended').items()
        for i in resp:
            if i._json['in_reply_to_status_id'] == self.__tweetMain:
                at = f'''@{i._json['user']['screen_name']}\n'''
                if i._json['user']['id'] not in self.__started.keys():
                    move = i._json['full_text'].replace(f'@{self.__un}','').replace(' ','').replace('\n','')
                    if len(move) == 1:                        
                        s = '''1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9'''
                        s = self.__player(s,move)
                        if self.__error == 0:
                            s = self.__computer(s)
                            while True:
                                try:
                                    self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])
                                    for tweet in self.__api.user_timeline(screen_name = self.__un,count=1):
                                        json = tweet._json
                                    break
                                except:
                                    pass
                            self.__started[i._json['user']['id']] = 0
                            new = threading.Thread(target = self.__run, args = [json])
                            new.start()
                            
                    else:
                        s = '''Error!'''
                        self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])

                else:
                    s = '''You already have a game started...'''
                    self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])
                self.__tweetLast = i._json['id']+1
        jsonLast = {'last':self.__tweetLast}
        with open('C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport/tweetLast.json', 'w') as fp:
            json1.dump(jsonLast, fp)
                
    def __run(self,json):
        bb = 0
        while True:
            if self.__active == True:
                if self.__condition == 'start':
                    self.__started[json['in_reply_to_user_id']] += 1
                    if self.__started[json['in_reply_to_user_id']] < 21:
                        self.__tweetLast = json['id']
                        print('listening {}-{}...'.format(json['in_reply_to_user_id'],self.__started[json['in_reply_to_user_id']]))
                        resp = tweepy.Cursor(self.__api.search, q=f'@{self.__un}', since_id=self.__tweetLast, tweet_mode='extended').items()
                        for i in resp:
                            if i._json['in_reply_to_status_id'] == json['id']:
                                self.__tweetLast = i._json['id']
                                self.__started[json['in_reply_to_user_id']] = 0
                                at = f'''@{i._json['user']['screen_name']}\n'''
                                s = json['text'].replace(f"@{i._json['user']['screen_name']}",'')
                                move = i._json['full_text'].replace(f'@{self.__un}','').replace(' ','').replace('\n','')
                                
                                if len(move) == 1:  
                                    s = self.__player(s, move)
                                    if self.__winner == False:
                                        if self.__error == 0:
                                            s = self.__computer(s)
                                            if self.__winner == True:
                                                self.__api.update_status(at+s+self.__winSTR,in_reply_to_status_id = i._json['id'])
                                                bb = 2
                                            else:
                                                while True:
                                                    try:
                                                        self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])
                                                        for tweet in self.__api.user_timeline(screen_name = self.__un,count=1):
                                                            json = tweet._json
                                                        break
                                                    except:
                                                        pass
                                        else:
                                            s = 'Try using a valid move!'
                                            self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])
                                    else:
                                        self.__api.update_status(at+s+self.__winSTR,in_reply_to_status_id = i._json['id'])
                                        bb = 2
                                else:
                                    s = 'Try using a valid move!'
                                    self.__api.update_status(at+s,in_reply_to_status_id = i._json['id'])
                                break
                        if bb == 2:
                            break
                        time.sleep(15)
                    else:
                        self.__api.update_status(f'@{self.__un} Game Abandoned',in_reply_to_status_id = json['id'])
                        del self.__started[json['in_reply_to_user_id']]
                        break
                else:
                    print('Paused...')
            else:
                break
                
    def __player(self,s,move):
        self.__winSTR = ''
        moves = self.__available(s)
        if move in moves:
            self.__error = 0
            s = s.replace(move,self.__o) 
            self.__winner = self.__win(s)
            if self.__winner == True:
                self.__winSTR = '\nPlayer Won!'
            return s.replace(move,self.__o)            
        self.__error = 1
        return 'Invalid Move! Start a New Game!'
    def __computer(self,s):
        self.__winSTR = ''
        moves = self.__available(s)
        pick = random.choice(moves)
        s = s.replace(pick,self.__x)
        self.__winner = self.__win(s)
        if self.__winner == True:
            self.__winSTR = '\nComputer Won!'
        if len(moves) == 1:
            self.__winSTR = '\nTie!'
            self.__winner = True
        return s
    
    def __str_to_df(self,str):
        df = pd.DataFrame([[0,0,0],[0,0,0],[0,0,0]])
        str = str.replace('\n','').replace('+','').replace('-','').replace('|','').replace(' ','').replace('️','')
        count = 0
        str = list(str)
        for i in [0,1,2]:
            for j in [0,1,2]:
                df.iloc[i,j]=str[count]
                count+=1
        return df
    
    
    def __win(self,game):
        game = self.__str_to_df(game)
        def all_same(l):
            if l.count(l[0]) == len(l) and l[0] != '0':
                return True
            else:
                return False
        # horizontal
        row = 0
        for row in game:
            row = game[row]
            if all_same(row.tolist()):
                return True 
        # vertical
        for col in range(len(game[0])):
            check = []
            for row in game:
                row = game[row]
                check.append(row[col])
            if all_same(check):
                return True
        # / diagonal
        diags = []
        for idx, reverse_idx in enumerate(reversed(range(len(game)))):
            diags.append(game[idx][reverse_idx])
        if all_same(diags):
            return True
        # \ diagonal
        diags = []
        for ix in range(len(game)):
            diags.append(game[ix][ix])
        if all_same(diags):
            return True
        return False
            
            
if __name__ == '__main__':    
    x = ticTaco('C:\\Users\\Conor\\Desktop')
        
    







