# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 15:12:06 2020

@author: Conor
"""
K = {'APIK' : 'ivFlGnWs5ayuV2fVoxl028nZc',
        'APISK' : '6j87ffgTzROQKgBl4TGA9MieIyLFsCqPmQHuVLxhfq7LdbYJHc',
        'BK' : 'AAAAAAAAAAAAAAAAAAAAAC0JJQEAAAAAFhhln8yZgzK4E4fFtOrHf1QLQbI%3D7cTnSt959IaSztufPY9MVkutPzHg16U7MgurgHXfldaQR2ood3',
        'AT' : '1321210238373109762-k87o7BGrrdFyNOBbW1NYagty4E1V8M',
        'ATS' : '8VrmLI98WVEmxymu5AfHsDlEJN5HC9oGZRkphllLZvf5i'
        }

    
import os
import json   
import tweepy
import time
import datetime as dt
import random
import threading
import re
import pandas as pd
class tweeter():
    def __init__(self,keys):
        self.__known = {}
        self.__queue = []
        self.__keys = keys
        self.__initDate = dt.date.today().strftime("%B %d, %Y")
        self.__start_from_break = False
        self.__active = True
        self.__condition = 'start'
        self.__X = 'X' #computer
        self.__O = 'O' #player
        self.scores = {self.__O:1,self.__X:-1,'tie':0}
        self.__setupAPI()
        self.__postGame()
        self.monitor = threading.Thread(target = self.__monitorDM, args = ())
        self.main = threading.Thread(target = self.__main, args = ())
        self.rank = threading.Thread(target = self.__rankings, args = ())
        self.rank.start()
        self.monitor.start()
        self.main.start()
    def __setupAPI(self):
        keys = self.__keys
        try:
            self.__apik = keys['APIK']
            self.__apisk = keys['APISK']
            self.__bk = keys['BK']
            self.__at = keys['AT']
            self.__ats = keys['ATS']
            auth = tweepy.OAuthHandler(self.__apik, self.__apisk)
            auth.set_access_token(self.__at, self.__ats)
            self.__api = tweepy.API(auth,wait_on_rate_limit =True,wait_on_rate_limit_notify=(True))
            self.__un = self.__api.me().screen_name
            self.__id = self.__api.me().id
            print('API Launching...')
        except:
            print('ERROR IN LAUNCHING API!')
    def __rankings(self):
        time.sleep(.1)
        print('Tracking Ranks!')
        while self.__active:
            if dt.datetime.now().strftime("%H:%M") == '11:30' and dt.datetime.today().weekday()==2:
                if len(self.__known.keys())>0:
                    games = {}
                    df = pd.DataFrame()
                    for i in self.__known.keys():
                        games[i] = {'wins':self.__known[i]['scores'][2],'ties':self.__known[i]['scores'][2],'games':self.__known[i]['scores'][2]}
                    df = df.from_dict(games,orient='index')
                    df = df.sort_values(['wins','games'],ascending=False)
                    df.index = df.index.set_names(['USER'])
                    df = df.reset_index(drop=False)
                    df.index = df.index.set_names(['PLACE'])
                    df = df.reset_index(drop=False)
                    df['PLACE'] = df['PLACE']+1
                    df = df[:3]
                    df['string'] = df.apply(lambda x: f"{x['PLACE']}{'st' if (x['PLACE']==1) else 'nd' if (x['PLACE']==2) else 'rd'} Place: @{x['USER']} with {x['wins']} {'wins' if x['wins'] != 1 else 'win'} from {x['games']} {'games' if x['games'] != 1 else 'game'}.",axis=1)
                    liststring = df['string'].tolist()
                    if len(liststring) > 0:
                        string = '\n'.join(liststring)
                        sday = self.__initDate
                        tweet = f"Rankings from {sday} to {dt.date.today().strftime('%B %d, %Y')}:\n\n{string}"
                        self.__api.update_status(tweet)
                time.sleep(60*60*24)
    def __flipActive(self):
        if self.__active == True:
            self.__active = False
        else:
            self.__active == True
    def __monitorDM(self):
        print('Monitor Launched...')
        while True:
            dms = self.__api.list_direct_messages(1) # Pull one of the new DMs
            for m in dms:
                nc = m.message_create['message_data']['text'].replace(' ','').replace('\n','').lower() # What is the text in the DM? Remove spaces and make all lowercase.
                t = m.message_create['target']['recipient_id']
                s = m.message_create['sender_id']
            if s==t: # Make sure this is a DM to myself. (High Risk for DDOS)
                if nc == 'start':
                    if self.__condition == nc and self.__active == True:
                        self.__api.send_direct_message(self.__id, 'Ignored, it is already started...') 
                        self.__changeCondition(nc)
                    elif self.__start_from_break == False:
                        if self.__active == False:
                            self.main.start()
                            self.rank.start()
                            self.__flipActive()
                        self.__api.send_direct_message(self.__id, 'System told to start...') 
                        self.__changeCondition(nc)
                    else:
                        self.__postGame()
                        self.__start_from_break = False
                        self.__flipActive()
                        self.__api.send_direct_message(self.__id, 'System initialized...')
                        self.__changeCondition(nc)
                elif nc == 'stop' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to stop...') # Verify command change.
                    self.__changeCondition(nc)
                elif nc == 'break' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to break...') # Verify command change.
                    self.__start_from_break = True
                    self.__flipActive()
                    self.__changeCondition(nc)
                elif nc == 'critstop':
                    self.__flipActive()
                    nc = 'break'
                    self.__api.send_direct_message(self.__id, 'System told to CRITSTOP!!!')
                    self.__changeCondition(nc)
                    break
            else:
                self.__api.create_block(s)
            time.sleep(60) 
    def __changeCondition(self,nc):
        self.__api.list_direct_messages(1); # I've sent myself a DM so I want to remove it from the queue
        self.__condition = nc
        time.sleep(60) 
    def __postGame(self): # See if a game exists
        def deleteTID(tid):
            self.__api.destroy_status(tid)
        list(map(lambda x: deleteTID(x._json['id']),tweepy.Cursor(self.__api.user_timeline).items()))
        self.__api.update_status('''Respond with a number to play!\n1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9''')
        tweetMain = self.__grabLastID()
        self.__tweetMain = tweetMain
        self.__tweetLast = tweetMain
    def __grabLastID(self):
        nJSON = self.__api.user_timeline(screen_name = self.__un,count=1)
        for i in nJSON:
            return i._json['id']
    def __refreshCursor(self):
        self.__resp = tweepy.Cursor(self.__api.search, q=f'@{self.__un}', since_id=self.__tweetLast, tweet_mode='extended')
    def __updateLast(self,nid):
        if nid > self.__tweetLast:
            self.__tweetLast = nid
    def __trimJSON(self,js):
        self.__updateLast(js['id'])
        return {'un':js['user']['screen_name'],
              'tid':js['id'],
              'rid':js['in_reply_to_status_id'],
              's':js['full_text'].replace(f"@{js['in_reply_to_screen_name']}",'').replace(' ','').replace('\n',''),
              'ls':len(js['full_text'].replace(f"@{js['in_reply_to_screen_name']}",'').replace(' ','').replace('\n',''))}
    def __main(self):
        t = threading.Thread(target = self.__tweets,args=[])
        t.start()
        while self.__active:
            if self.__condition == 'start':
                self.__refreshCursor()
                tweets = reversed(list(map(lambda x: self.__trimJSON(x._json),self.__resp.items())))
                list(map(lambda x: self.__handle(x),tweets))
                list(map(lambda x: self.__monitor(x),self.__known.keys()))
                time.sleep(5)
        self.__api.send_direct_message(self.__id, 'Crashed...') # Verify command change.
        self.__changeCondition('break')
    def __reset(self,key):
        self.__known[key]['moves'] = [1,2,3,4,5,6,7,8,9]
        self.__known[key]['moved'] = {}
        self.__known[key]['status'] = 'live'
        self.__known[key]['inactive'] = 0
        self.__known[key]['rid'] = self.__tweetMain
    def __start(self,t):
        self.__known[t['un']] =\
            {'un':f"@{t['un']}",
             'tid':t['tid'],
             'rid':123,
             's':t['s'],
             'tweet':[],
             'status':'live',
             'scores':[0,0,0],
             'count_error':0}
        
        if t['ls'] == 1:
            try:
                self.__reset(t['un'])
                int(t['s'])
                self.__player(t['un'])
            except:
                self.__known[t['un']]['status'] = 'dead'
                self.__known[t['un']]['tweet'].append({'tid':t['tid'],'un':t['un'],'s':'Please respond with a Number!','code':'error'})
    def minimax(self,board,depth,isMax):#Let's kick some tic tac ASS
        moves = list(set(re.sub('\D','',board)))
        result = self.__win(board)
        if result[1] != '':
            return self.scores[result[1]]
        if isMax:
            maxEval = -9999
            for i in moves:
                nb = board.replace(i,self.__O)
                score = self.minimax(nb,depth-1,False)
                maxEval = max(score,maxEval)
            return maxEval
        else:
            maxEval = 9999
            for i in moves:
                nb = board.replace(i,self.__X)
                score = self.minimax(nb,depth-1,True)
                maxEval = min(score,maxEval)
            return maxEval
    def __player(self,key):
        if int(self.__known[key]['s']) in self.__known[key]['moves']:
            self.__known[key]['moved'][self.__known[key]['s']] = self.__O
            self.__known[key]['moves'].remove(int(self.__known[key]['s']))
            self.__known[key]['status'] = 'live'
            self.__computer(key)
        else:
            self.__known[key]['tweet'].append({'tid':self.__known[key]['tid'],'un':key,'s':'Not a Valid Move, try again!','code':'error'})
    def __computer(self,key):
        board = '1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9'
        self.__known[key]['rid'] = 123
        for i,j in self.__known[key]['moved'].items():
            board = board.replace(i, j)
        win = self.__win(board)
        if win[1] == '':
            if random.random() < 0.65:
                m1 = 9999
                for i in self.__known[key]['moves']:
                    nb = board.replace(str(i),self.__X)
                    score = self.minimax(nb,4,True)
                    if score < m1:
                        m1 = score
                        move = i  
            else:
                move = random.choice(self.__known[key]['moves'])
            self.__known[key]['moves'].remove(move)
            self.__known[key]['moved'][str(move)] = self.__X
            for i,j in self.__known[key]['moved'].items():
                board = board.replace(i, j)
            win = self.__win(board)
            if win[1] == '':
                self.__known[key]['tweet'].append({'tid':self.__known[key]['tid'],'un':key,'s':board,'code':'valid'})
            else:
                self.__known[key]['scores'][2]+=1
                self.__known[key]['tweet'].append({'tid':self.__known[key]['tid'],'un':key,'s':f"{board}\n\n I won {win[0]}! You've won {self.__known[key]['scores'][0]} out of {self.__known[key]['scores'][2]} games, {self.__known[key]['scores'][1]} Ties.",'code':'winner'})
        elif win[1] == 'tie':
            self.__known[key]['scores'][1]+=1 #ties
            self.__known[key]['scores'][2]+=1 #games
            self.__known[key]['tweet'].append( {'tid':self.__known[key]['tid'],'un':key,'s':f"{board}\n\n We Tied! You've won {self.__known[key]['scores'][0]} out of {self.__known[key]['scores'][2]} games, {self.__known[key]['scores'][1]} Ties.",'code':'winner'})
        else:
            self.__known[key]['scores'][2]+=1
            self.__known[key]['scores'][0]+=1 #wins
            self.__known[key]['tweet'].append({'tid':self.__known[key]['tid'],'un':key,'s':f"{board}\n\n You won {win[0]}! You've won {self.__known[key]['scores'][0]} out of {self.__known[key]['scores'][2]} games, {self.__known[key]['scores'][1]} Ties.",'code':'winner'})
    def __same(self,items):
        return all(x == items[0] for x in items)
    def __win(self,board):
        how = ''
        char = ''
        win = re.sub('\W','',board)
        for i in range(0,3):
            set1 = win[3*i:3*(i+1)]
            if self.__same(set1):
                how = 'Horizontally'
                char = set1[0]
                return how,char
        for i in range(0,3):
            set1 = win[0+i:1+i]+win[3+i:4+i]+win[6+i:7+i]
            if self.__same(set1):
                how = 'Vertically'
                char = set1[0]
                return how,char
        di1 = win[0:1]+win[4:5]+win[8:9]
        di2 = win[2:3]+win[4:5]+win[6:7]
        if self.__same(di1) or self.__same(di2):
            how = 'Diagonally'
            char = win[4:5]
            return how,char
        if len(list(set(re.sub('\D','',board)))) == 0:
            return '','tie'
        return how,char
    def __handle(self,t):
        key = t['un']
        if key not in self.__known.keys():
            if t['rid'] == self.__tweetMain:  
                self.__start(t)
        else:
            if self.__known[key]['rid'] == t['rid']:
                self.__known[key]['s'] = t['s']
                self.__known[key]['tid'] = t['tid']
                self.__player(t['un'])
            else:
                if self.__known[key]['status'] == 'dead':
                    self.__known[key]['tweet'].append( {'tid':t['tid'],'un':key,'s':'Start a game by responding to the inital tweet!','code':'error'})
                elif self.__known[key]['status'] == 'inactive':
                    self.__known[key]['tweet'].append({'tid':t['tid'],'un':key,'s':'This game is marked as inactive; Start a new game by responding to the inital tweet!','code':'error'})
                else:
                    self.__known[key]['tweet'].append({'tid':t['tid'],'un':key,'s':'You already have an active game, finish that one first!','code':'error'})
    def __monitor(self,key):
        if self.__known[key]['tweet'] == []:
            if self.__known[key]['status'] == 'live':
               self.__known[key]['inactive'] += 1 
               if self.__known[key]['inactive'] > 120:
                   self.__reset(key)
                   self.__known[key]['status'] = 'inactive'
                   self.__known[key]['scores'][2] += 1

        else:
            self.__queue.extend(self.__known[key]['tweet'])
    def __tweets(self):
        while self.__active:
            if self.__condition == 'start':
                try:
                    copy = self.__queue.copy()
                    for i in copy:
                        key = i['un']
                        tweet = f"@{i['un']}\n{i['s']}"
                        self.__api.update_status(tweet,in_reply_to_status_id = i['tid'])
                        rid = self.__grabLastID()
                        self.__queue.remove(i)
                        if i['code'] == 'error':
                            self.__known[key]['count_error']+=1
                            self.__known[key]['inactive']+=1
                        elif i['code'] == 'winner':
                            self.__reset(key)
                            self.__known[key]['status'] = 'dead'
                        else:
                            self.__known[key]['rid'] = rid
                            self.__known[key]['inactive']=0
                        if self.__known[key]['count_error'] > 15:
                            self.__api.create_block(key)
                        self.__known[key]['tweet'] = []
                        time.sleep(.25)
                except:
                    self.__resp = tweepy.Cursor(self.__api.search, q=tweet, since_id=self.__tweetMain, tweet_mode='extended')
                    for i in self.__resp.items():
                        self.__api.destroy_status(i._json['id'])
        self.__api.send_direct_message(self.__id, 'Crashed...') # Verify command change.
        self.__changeCondition('break')
tweeter(K)