# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 17:02:52 2020

@author: Conor
"""

#Let's use a more dictionary approach


#I think i want three systems,

# 1. Look for new games
# 2. Look for existing games
# 3. Send tweets to existing games

# What does a game exist of:
    # A game has a GAME_ID; this will be the user ID.
    # A game has a UN; this is the username included in computer responses
    # A game has a TID which is the tweet ID to respond to
    # A game has a S, which is the latest tweet content to the computer
    # A game has a RID; this will be the tweet we are looking for a response to.
    # A game has a list of moves left, this is the moves that are able to be played.
    # A game has a list of moves that the player has made, this is moves twitter user has made.
    # A game has a list of moves that the computer has made, this is moves computer has made.
    # A game has a last Time, this is the time a move was made last.
    # a game has a status, should this be ignored, should the person and computer make a move?
    # A game has a number of wins, this will say who has won the most times.
K = {'APIK' : 'ivFlGnWs5ayuV2fVoxl028nZc',
        'APISK' : '6j87ffgTzROQKgBl4TGA9MieIyLFsCqPmQHuVLxhfq7LdbYJHc',
        'BK' : 'AAAAAAAAAAAAAAAAAAAAAC0JJQEAAAAAFhhln8yZgzK4E4fFtOrHf1QLQbI%3D7cTnSt959IaSztufPY9MVkutPzHg16U7MgurgHXfldaQR2ood3',
        'AT' : '1321210238373109762-k87o7BGrrdFyNOBbW1NYagty4E1V8M',
        'ATS' : '8VrmLI98WVEmxymu5AfHsDlEJN5HC9oGZRkphllLZvf5i'
        }

import os
import json   
import tweepy
import time
import random
import threading
import re
class tweeter():
    def __init__(self):
        self.__known = {}
        self.__q = []
        self.__X = 'X'
        self.__O = 'O'
        self.__active = False
        self.__condition = ''
        self.__path = 'C:\\Users\\Conor\\Desktop'
        self.__setupAPI()
        t = threading.Thread(target = self.__monitorDM,args = [])
        t.start()
    def __monitorDM(self):
        print('Monitor Launched...')
        while True:
            dms = self.__api.list_direct_messages(1) # Pull one of the new DMs
            for m in dms:
                nc = m.message_create['message_data']['text'].replace(' ','').replace('\n','').lower() # What is the text in the DM? Remove spaces and make all lowercase.
                t = m.message_create['target']['recipient_id']
                s = m.message_create['sender_id']
            if s==t: # Make sure this is a DM to myself. (High Risk for DDOS) would likely make a burner to send all the requests if i had issues
                if nc == 'start':
                    if self.__active == False:
                        self.__api.send_direct_message(self.__id, 'System told to start...') 
                        t1 = threading.Thread(target = self.__listen, args = ())
                        t1.start()
                        self.__changeCondition(nc)
                    else:
                        self.__api.send_direct_message(self.__id, 'System already initialized...')
                        nc = self.__condition
                        self.__changeCondition(nc)
                elif nc == 'stop' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to stop...') # Verify command change.
                    self.__flipActive()
                    self.__changeCondition(nc)
                elif nc == 'break' and self.__active == True:
                    self.__api.send_direct_message(self.__id, 'System told to break...') # Verify command change.
                    self.__flipActive()
                    self.__changeCondition(nc)
                elif nc == 'critstop':
                    nc = 'break'
                    self.__api.send_direct_message(self.__id, 'System told to CRITSTOP!!!')
                    self.__flipActive()
                    self.__changeCondition(nc)
                    break
            time.sleep(60) 
    def __changeCondition(self,nc):
        self.__api.list_direct_messages(1); # I've sent myself a DM so I want to remove it from the queue
        self.__condition = nc
        time.sleep(60) 
    def __flipActive(self):
        if self.__active == True:
            self.__active = False
        else:
            self.__active = True
    def __setupAPI(self):
        cwd = os.getcwd()
        os.chdir(self.__path)
        with open('jsonkeys.json', 'r') as fp:
            keys = json.load(fp)
        try:
            self.__apik = keys['APIK']
            self.__apisk = keys['APISK']
            self.__bk = keys['BK']
            self.__at = keys['AT']
            self.__ats = keys['ATS']
            auth = tweepy.OAuthHandler(self.__apik, self.__apisk)
            auth.set_access_token(self.__at, self.__ats)
            
            self.__api = tweepy.API(auth,wait_on_rate_limit =True,wait_on_rate_limit_notify=(True))
            self.__un = self.__api.me().screen_name
            self.__id = self.__api.me().id
            print('API Launching...')
        except:
            print('ERROR IN LAUNCHING API!')
        os.chdir(cwd)
    def __checkGame(self): # See if a game exists
        file = 'C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport'
        try:
            with open(f'{file}/tweetMain.json', 'r') as fp:
                jsonTweet = json.load(fp)
            with open(f'{file}/tweetLast.json', 'r') as fp:
                jsonLast = json.load(fp)
            # with open(f'{file}/players.json', 'r') as fp:
            #     self.__known = json.load(fp)

            self.__tweetMain = jsonTweet['tweet']
            self.__tweetLast = jsonLast['last']
            self.__api.get_status(self.__tweetMain)
            print('Running off existing Tweet...')
        except:
            self.__updateStatus('''Respond with a number to play!\n1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9''')
            tweetMain = self.__grabLastID()
            jsonTweet = {'tweet':tweetMain}
            jsonLast = {'last':tweetMain}
            with open(f'{file}/tweetMain.json', 'w') as fp:
                json.dump(jsonTweet, fp)
            with open(f'{file}/tweetLast.json', 'w') as fp:
                json.dump(jsonLast, fp)
            self.__known = {}
            self.__tweetMain = jsonTweet['tweet']
            self.__tweetLast = jsonLast['last']
            print("Tweet doesnt exist... Creating new tweet!")
    def __refreshCursor(self):
        self.__resp = tweepy.Cursor(self.__api.search, q=f'@{self.__un}', since_id=self.__tweetLast, tweet_mode='extended')
    def __updateStatus(self,s):
        self.__api.update_status(s)
    def __updateLast(self,nid):
        if nid > self.__tweetLast:
            self.__tweetLast = nid
    def __grabLastID(self):
        def getJSON(x):
            return x._json
        nJSON = list(map(lambda x: getJSON(x),self.__api.user_timeline(screen_name = self.__un,count=1)))
        return nJSON[0]['id']
    def __tweet(self,json):
        self.__updateLast(json['id'])
        return {'un':json['user']['screen_name'],
              'tid':json['id'],
              'rid':json['in_reply_to_status_id'],
              's':json['full_text'].replace(f"@{json['in_reply_to_screen_name']}",'').replace(' ','').replace('\n',''),
              'ls':len(json['full_text'].replace(f"@{json['in_reply_to_screen_name']}",'').replace(' ','').replace('\n','')),
              'json':json}
    
    def __listen(self):
        self.__checkGame()
        self.__flipActive()
        print('Listening for Tweets...')
        t = threading.Thread(target = self.__tweetQueue,args = [])
        t.start()
        while self.__active:
            try:
                print(self.__condition)
                if self.__condition == 'start':
                    tweets = []
                    self.__refreshCursor()     
                    tweets = reversed(list(map(lambda x: self.__tweet(x._json),self.__resp.items())))
                    list(map(lambda x: self.__handleResp(x),tweets));
                    list(map(lambda x: self.__monitor(x),self.__known.keys()))
                    file = 'C:/Users/Conor/Desktop/code/public/twitterAPI/ticTaco/jsonSupport'
                    with open(f'{file}/tweetLast.json', 'w') as fp:
                        json.dump(self.__tweetLast, fp)
                    with open(f'{file}/players.json', 'w') as fp:
                        json.dump(self.__known, fp)
                    time.sleep(10)
            except:
                print('error1')
                self.__flipActive()
        print('dead1')
    def __newGame(self,js):
        self.__known[js['un']]= \
            {'tid':js['tid'],
             'rid':123,
             's':js['s'],
             'tweet':{},
             'status':'',
             'moves':[1,2,3,4,5,6,7,8,9],
             'moved':{},
             'inactive':0,
             'record':[0,0,0]
             }
        self.__player(js['un'])
    def __reset(self,key):
        self.__known[key]['status'] = ''
        self.__known[key]['moves'] = [1,2,3,4,5,6,7,8,9]
        self.__known[key]['moved'] = {}
        self.__known[key]['inactive'] = 0
    def __win(self,board):
        win = re.sub('\W','',board)
        winner = ''
        how = ''
        for i in range(0,3):
            if self.__O*3 in win[3*i:3*(i+1)]:
                winner = self.__O
                how = 'Horizontally'
            if self.__X*3 in win[3*i:3*(i+1)]:
                winner = self.__X
                how = 'Horizontally'
        for i in range(0,3):
            if win[0+i:1+i] == win[3+i:4+i] and win[0+i:1+i] == win[6+i:7+i] and win[0+i:1+i] == self.__O:
                winner = self.__O
                how = 'Vertically'
            if win[0+i:1+i] == win[3+i:4+i] and win[0+i:1+i] == win[6+i:7+i] and win[0+i:1+i] == self.__X:
                winner = self.__X
                how = 'Vertically'
        di1 = win[0:1]+win[4:5]+win[8:9]
        di2 = win[2:3]+win[4:5]+win[6:7]
        if self.__O*3 == di1 or self.__O*3 == di2:
            winner = self.__O
            how = 'Diagonally'
        if self.__X*3 == di1 or self.__X*3 == di2:
            winner = self.__O
            how = 'Diagonally'
        return winner,how
    def __player(self,key):
        try:
            if int(self.__known[key]['s']) in self.__known[key]['moves']:
                self.__known[key]['moved'][self.__known[key]['s']] = self.__X
                self.__known[key]['moves'].remove(int(self.__known[key]['s']))
                self.__computer(key)
            else:
                self.__known[key]['status'] = 'tweet'
                self.__known[key]['tweet'] = {'un':key,'id':self.__known[key]['tid'],'s':'Not a Valid Move!','code':'error'}
        except:
            self.__known[key]['status'] = 'tweet'
            self.__known[key]['tweet'] = {'un':key,'id':self.__known[key]['tid'],'s':'Input must be an Integer!','code':'error'}
    def __computer(self,key):
       #check for a winner
        board = '1 |  2  | 3\n- + - + -\n4 |  5  | 6\n- + - + -\n7 |  8  | 9'
        for j,k in self.__known[key]['moved'].items():
           board = board.replace(j,k)
        winner,how = self.__win(board)
        if winner != '':
            self.__known[key]['record'][1] += 1
            self.__known[key]['record'][2] += 1
            self.__known[key]['status'] = 'tweet'
            self.__known[key]['tweet'] = {'un':key,'id':self.__known[key]['tid'],'s':board+f'\n\n You won {how}! You have won {self.__known[key]["record"][1]} times out of {self.__known[key]["record"][2]} games!','code':'winner'}
        else:
            moves = self.__known[key]['moves']
            pick = random.choice(moves)
            self.__known[key]['moved'][str(pick)] = self.__O
            self.__known[key]['moves'].remove(pick)
            self.__known[key]['status'] = 'tweet'
            for j,k in self.__known[key]['moved'].items():
                board = board.replace(j,k)
            #Check for a winner
            winner,how = self.__win(board)
            if winner != '':
                self.__known[key]['record'][0] += 1
                self.__known[key]['record'][2] += 1
                self.__known[key]['status'] = 'tweet'
                self.__known[key]['tweet'] = {'un':key,'id':self.__known[key]['tid'],'s':board+f'\n\n You LOST {how}! You have won {self.__known[key]["record"][1]} times out of {self.__known[key]["record"][2]} games!','code':'winner'}
            else:
                self.__known[key]['tweet'] = {'un':key,'id':self.__known[key]['tid'],'s':board,'code':''}
        
        
    def __handleResp(self,js):
        if js['un'] in self.__known:
            if js['rid'] == self.__known[js['un']]['rid'] and self.__known[js['un']]['status']!='dead':
                self.__known[js['un']]['tid'] = js['tid']
                self.__known[js['un']]['s'] = js['s']
                self.__player(js['un'])
            elif js['rid'] == self.__tweetMain and self.__known[js['un']]['status']=='dead':
                self.__known[js['un']]['tid'] = js['tid']
                self.__known[js['un']]['s'] = js['s']
                self.__reset(js['un'])
                self.__player(js['un'])
            elif self.__known[js['un']]['status']=='dead':
                self.__known[js['un']]['tweet'] = {'un':js['un'],'id':js['tid'],'s':'This game is marked as abandoned, start a new one!','code':'abandoned'}
                self.__known[js['un']]['status'] = 'tweet'
            elif js['rid'] == self.__tweetMain:
                self.__known[js['un']]['tweet'] = {'un':js['un'],'id':js['tid'],'s':'Finish your current game!','code':'error'}
                self.__known[js['un']]['status'] = 'tweet'
        else:
            if js['rid'] == self.__tweetMain:
                print(f'UNKNOWN: Start Game for {js["un"]}')
                self.__newGame(js)
     
    def __monitor(self,key):
        js = self.__known[key]
        if js['status'] == 'tweet':
            self.__q.append(self.__known[key]['tweet'])
        elif js['status'] == 'look':
            self.__known[key]['tweet'] = {}
            self.__known[key]['inactive'] += 1
            if self.__known[key]['inactive'] > 20:
                self.__known[key]['status'] = 'dead'
                self.__known[key]['record'][0] += 1
                self.__known[key]['record'][2] += 1
        elif js['status'] == 'dead':
            self.__known[key]['tweet'] = {}

    def __tweetQueue(self):
        print('Accepting Queue...')
        while self.__active:
            try:
                if self.__condition == 'start':
                    copy = self.__q.copy()
                    for i in copy:
                        tweet = f"@{i['un']}\n{i['s']}"
                        self.__api.update_status(tweet,in_reply_to_status_id = i['id'])
                        if i['code'] != 'error':
                            self.__known[i['un']]['rid'] = self.__grabLastID()
                            self.__known[i['un']]['status'] = 'look'
                            self.__known[i['un']]['inactive'] = 0
                        if i['code'] == 'abandoned':
                            self.__known[i['un']]['status'] = 'dead'
                        if i['code'] == 'winner':
                            self.__known[i['un']]['status'] = 'dead'
                        self.__q.remove(i)
                        time.sleep(.25)
            except:
                print('error2')
                self.__flipActive()
        print('dead2')

tweeter()






